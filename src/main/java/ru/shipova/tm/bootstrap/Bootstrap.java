package ru.shipova.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.api.repository.IProjectRepository;
import ru.shipova.tm.api.repository.ITaskRepository;
import ru.shipova.tm.api.repository.IUserRepository;
import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.CommandCorruptException;
import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.repository.TaskRepository;
import ru.shipova.tm.repository.UserRepository;
import ru.shipova.tm.service.ProjectService;
import ru.shipova.tm.service.TaskService;
import ru.shipova.tm.service.TerminalService;
import ru.shipova.tm.service.UserService;

import java.util.*;

/**
 * Класс загрузчика приложения
 */

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository, taskRepository);
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository, projectRepository);
    @NotNull
    private final TerminalService terminalService = new TerminalService(this);
    @NotNull
    private final IUserRepository userRepository = new UserRepository();
    @NotNull
    private final IUserService userService = new UserService(userRepository);

    //At runtime, find all classes in a Java application that extend a AbstractCommand class
    private final Set<Class<? extends AbstractCommand>> commandSet =
            new Reflections("ru.shipova.tm").getSubTypesOf(AbstractCommand.class);

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void init(){
        loadCommands();

        System.out.println("*** WELCOME TO TASK MANAGER ***");
        @NotNull String commandName = "";
        while (!"exit".equals(commandName)) {
            commandName = terminalService.nextLine();
            if (!commands.containsKey(commandName)) {
                System.out.println("WRONG COMMAND. ENTER 'help' TO GET ALL AVAILABLE COMMANDS.");
            }
            try {
                execute(commandName);
            } catch (Exception e) {
                System.out.println("CANNOT DO COMMAND.");
                System.out.println(e.getMessage());
            }
        }
    }

    private void loadCommands(){
        for (@NotNull final Class commandClass : commandSet) {
            try {
                if (AbstractCommand.class.isAssignableFrom(commandClass)) registry((AbstractCommand) commandClass.newInstance());
            } catch (CommandCorruptException | InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private void registry(@Nullable final AbstractCommand command) throws CommandCorruptException {
        if (command == null) throw new CommandCorruptException();

        @Nullable final String commandName = command.getName(); //Command Line Interface
        @Nullable final String cliDescription = command.getDescription();
        if (commandName == null || commandName.isEmpty()) throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty()) throw new CommandCorruptException();

        command.setServiceLocator(this);
        commands.put(commandName, command);
    }

    private void execute(@Nullable final String commandName) throws Exception {
        if (commandName == null || commandName.isEmpty()) return;

        @Nullable final AbstractCommand abstractCommand = commands.get(commandName);
        if (abstractCommand == null) return;
        @Nullable final User currentUser = userService.getCurrentUser();

        if (currentUser == null) {
            if (abstractCommand.needAuthorize()) {
                System.out.println("ACCESS ERROR");
                return;
            } else {
                abstractCommand.execute();
            }
            return;
        }

        @Nullable final String role = currentUser.getRoleType() != null ? currentUser.getRoleType().name() : null;
        if (role == null || role.isEmpty()) return;
        if (RoleType.USER.name().equals(role)) {
            if (abstractCommand.isOnlyAdminCommand()) {
                System.out.println("ACCESS ERROR");
                return;
            } else abstractCommand.execute();
        }
        if (RoleType.ADMIN.name().equals(role)) {
            abstractCommand.execute();
        }
    }

    @NotNull
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @NotNull
    @Override
    public TerminalService getTerminalService() {
        return terminalService;
    }

    @NotNull
    @Override
    public IProjectService getIProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getITaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IUserService getIUserService() {
        return userService;
    }
}
