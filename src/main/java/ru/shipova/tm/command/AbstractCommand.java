package ru.shipova.tm.command;

import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;

@Setter
public abstract class AbstractCommand {
    protected @Nullable IServiceLocator serviceLocator;

    public abstract @Nullable String getName();

    public abstract @Nullable String getDescription();

    public abstract void execute() throws Exception;

    public abstract boolean needAuthorize();

    public boolean isOnlyAdminCommand() {
        return false;
    }
}
