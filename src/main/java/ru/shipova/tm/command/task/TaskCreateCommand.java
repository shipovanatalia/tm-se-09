package ru.shipova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.exception.ProjectDoesNotExistException;

public final class TaskCreateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            System.out.println("[TASK CREATE]");
            System.out.println("ENTER NAME:");
            @NotNull final ITaskService taskService = serviceLocator.getITaskService();
            @NotNull final String taskName = serviceLocator.getTerminalService().nextLine();
            System.out.println("ENTER PROJECT NAME:");
            @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();

            @Nullable final String userId = serviceLocator.getIUserService().getCurrentUser().getId();

            try {
                taskService.create(userId, taskName, projectName);
            } catch (ProjectDoesNotExistException e) {
                System.out.println("PROJECT DOES NOT EXISTS");
            }
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
