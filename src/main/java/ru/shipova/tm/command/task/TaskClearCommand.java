package ru.shipova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;

public final class TaskClearCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @NotNull final ITaskService taskService = serviceLocator.getITaskService();
            @NotNull final IUserService userService = serviceLocator.getIUserService();
            @Nullable final User currentUser = userService.getCurrentUser();
            @Nullable final String userId = currentUser != null ? currentUser.getId() : null;
            taskService.clear(userId);
            System.out.println("[ALL TASKS REMOVED]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }

}
