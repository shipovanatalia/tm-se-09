package ru.shipova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Exit from application.";
    }

    @Override
    public void execute(){
        System.exit(0);
    }

    @Override
    public boolean needAuthorize() {
        return false;
    }
}
