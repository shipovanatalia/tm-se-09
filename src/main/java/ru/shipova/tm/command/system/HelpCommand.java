package ru.shipova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute(){
        for (@NotNull final AbstractCommand command :
                serviceLocator.getTerminalService().getCommands()) {
            System.out.println(command.getName() + ": "
                    + command.getDescription());
        }
    }

    @Override
    public boolean needAuthorize() {
        return false;
    }
}
