package ru.shipova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.User;

public final class UserSetPasswordCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-set-password";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Set new password.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @NotNull final IUserService userService = serviceLocator.getIUserService();
            @Nullable final User currentUser = userService.getCurrentUser();
            if (currentUser == null) return;
            @Nullable String userLoginToUpdate = currentUser.getLogin();
            if (currentUser.getRoleType() == null) return;

            @Nullable final String roleOfCurrentUser = currentUser.getRoleType().name();

            if (RoleType.ADMIN.name().equals(roleOfCurrentUser)) {
                System.out.println("ENTER LOGIN OF USER TO UPDATE:");
                userLoginToUpdate = serviceLocator.getTerminalService().nextLine();
            }

            System.out.println("ENTER NEW PASSWORD");
            @NotNull final String password1 = serviceLocator.getTerminalService().nextLine();
            System.out.println("REPEAT NEW PASSWORD");
            @NotNull final String password2 = serviceLocator.getTerminalService().nextLine();
            if (password1.equals(password2)) {
                userService.setNewPassword(userLoginToUpdate, password1);
                System.out.println("[OK]");
            } else {
                System.out.println("PASSWORDS DO NOT MATCH.");
                execute();
            }
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
