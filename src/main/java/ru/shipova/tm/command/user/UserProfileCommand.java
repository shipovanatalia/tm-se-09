package ru.shipova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;

public final class UserProfileCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Get all information about user.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @NotNull final IUserService userService = serviceLocator.getIUserService();
            @Nullable final User currentUser = userService.getCurrentUser();
            if (currentUser == null) {
                System.out.println("PLEASE LOGIN TO GET INFORMATION");
                return;
            }
            System.out.println("[USER PROFILE]");
            System.out.println("USER LOGIN: " + currentUser.getLogin());
            System.out.println("USER ROLE TYPE: " + (currentUser.getRoleType() != null ? currentUser.getRoleType().displayName() : null));
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
