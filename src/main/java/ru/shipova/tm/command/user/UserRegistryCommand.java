package ru.shipova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;

public final class UserRegistryCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-registry";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Registry new user.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            System.out.println("[REGISTRY USER]");
            System.out.println("ENTER LOGIN");
            @NotNull final String login = serviceLocator.getTerminalService().nextLine();
            System.out.println("ENTER PASSWORD");
            @NotNull final String password = serviceLocator.getTerminalService().nextLine();
            System.out.println("ENTER ROLE OF USER");
            @NotNull final String roleType = serviceLocator.getTerminalService().nextLine();
            @NotNull final IUserService userService = serviceLocator.getIUserService();
            userService.registryUser(login, password, roleType);
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }

    @Override
    public boolean isOnlyAdminCommand() {
        return true;
    }
}
