package ru.shipova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.User;

public final class ProjectRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            System.out.println("[PROJECT REMOVE]");
            System.out.println("ENTER NAME OF PROJECT:");
            @NotNull final IProjectService projectService = serviceLocator.getIProjectService();
            @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();

            @NotNull final IUserService userService = serviceLocator.getIUserService();
            @NotNull final User currentUser = userService.getCurrentUser();
            @NotNull final String userId = currentUser.getId();

            projectService.remove(userId, projectName);
            System.out.println("[PROJECT " + projectName + " REMOVED]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
