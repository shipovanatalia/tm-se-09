package ru.shipova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.User;

import java.util.List;

public class ProjectSearchCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-search";
    }

    @Override
    public String getDescription() {
        return "Search project by part of name.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            System.out.println("[PROJECT SEARCH]");
            @NotNull final IProjectService projectService = serviceLocator.getIProjectService();
            @NotNull final String partOfData = serviceLocator.getTerminalService().nextLine();

            @NotNull final IUserService userService = serviceLocator.getIUserService();
            @NotNull final User currentUser = userService.getCurrentUser();
            @NotNull final String userId = currentUser.getId();

            @Nullable final List<Project> projectList = projectService.search(userId, partOfData);
            if (projectList == null || projectList.isEmpty()) {
                System.out.println("PROJECTS ARE NOT FOUND");
                return;
            }

            int index = 1;
            for (@NotNull final Project project : projectList) {
                System.out.println(index++ + ". " + project.getName());
            }
            System.out.println();
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
