package ru.shipova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.CommandCorruptException;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            System.out.println("[PROJECT LIST]");
            @NotNull final IProjectService projectService = serviceLocator.getIProjectService();
            @NotNull final IUserService userService = serviceLocator.getIUserService();
            @NotNull final User currentUser = userService.getCurrentUser();
            @NotNull final String userId = currentUser.getId();
            sortProjects(projectService, userId);
        }
    }

    private void sortProjects(@NotNull final IProjectService projectService, @NotNull final String userId) {
        if (serviceLocator != null) {
            System.out.println("PLEASE CHOOSE TYPE OF SORT OF PROJECTS: ");
            System.out.println("1. DATE OF CREATE;");
            System.out.println("2. DATE OF BEGIN;");
            System.out.println("3. DATE OF END;");
            System.out.println("4. STATUS.");
            System.out.println("ENTER NAME OF SORT OR NO");
            @NotNull final String typeOfSort = serviceLocator.getTerminalService().nextLine();
            @Nullable final List<Project> listProject = projectService.getListProject(userId);
            if (listProject == null) {
                System.out.println("LIST OF PROJECTS DOES NOT EXISTS.");
                return;
            }
            @Nullable List<Project> sortedList = null;
            try {
                sortedList = projectService.sort(typeOfSort, listProject);
            } catch (CommandCorruptException e) {
                System.out.println("WRONG COMMAND.");
                System.out.println();
                sortProjects(projectService, userId);
            }

            int index = 1;
            if (sortedList == null) return;

            for (@Nullable final Project project : sortedList) {
                if (project == null) return;
                System.out.println(index++ + ". " + project.getName());
            }
            System.out.println();
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
