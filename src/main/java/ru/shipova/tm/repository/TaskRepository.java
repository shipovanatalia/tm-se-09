package ru.shipova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.repository.ITaskRepository;
import ru.shipova.tm.comparator.ComparatorByDateOfBegin;
import ru.shipova.tm.comparator.ComparatorByDateOfCreate;
import ru.shipova.tm.comparator.ComparatorByDateOfEnd;
import ru.shipova.tm.comparator.ComparatorByStatus;
import ru.shipova.tm.constant.Status;
import ru.shipova.tm.constant.TypeOfSort;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.exception.CommandCorruptException;

import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public TaskRepository() {

        //Таски для проверки работы сортировщика
        final String task_1_Id = UUID.randomUUID().toString();
        final String task_1_userId = "1";
        final String task_1_projectId = UUID.randomUUID().toString();
        final String task_1_name = "task_1";
        final Status task_1_status = Status.IN_PROCESS;
        final Date task_1_date_of_begin = new Date(2019, 1, 1);
        final Date task_1_date_of_end = new Date(2019, 10, 1);
        getMap().put(task_1_Id, new Task(task_1_Id, task_1_name, task_1_projectId, task_1_userId, task_1_status, task_1_date_of_begin, task_1_date_of_end));

        final String task_2_Id = UUID.randomUUID().toString();
        final String task_2_userId = "1";
        final String task_2_projectId = UUID.randomUUID().toString();
        final String task_2_name = "task_2";
        final Status task_2_status = Status.READY;
        final Date task_2_date_of_begin = new Date(2019, 2, 1);
        final Date task_2_date_of_end = new Date(2019, 9, 1);
        getMap().put(task_2_Id, new Task(task_2_Id, task_2_name, task_2_projectId, task_2_userId, task_2_status, task_2_date_of_begin, task_2_date_of_end));

        final String task_3_Id = UUID.randomUUID().toString();
        final String task_3_userId = "1";
        final String task_3_projectId = UUID.randomUUID().toString();
        final String task_3_name = "task_3";
        final Status task_3_status = Status.PLANNED;
        final Date task_3_date_of_begin = new Date(2019, 3, 1);
        final Date task_3_date_of_end = new Date(2019, 8, 1);
        getMap().put(task_3_Id, new Task(task_3_Id, task_3_name, task_3_projectId, task_3_userId, task_3_status, task_3_date_of_begin, task_3_date_of_end));
    }

    @NotNull
    public List<Task> sort(@NotNull final String typeOfSort, @NotNull final List<Task> taskList) throws CommandCorruptException {
        if (TypeOfSort.NO.displayName().equals(typeOfSort.toUpperCase())) {
            return taskList;
        }
        if (TypeOfSort.STATUS.displayName().equals(typeOfSort.toUpperCase())) {
            @NotNull Comparator<Task> comparator = new ComparatorByStatus<Task>();
            taskList.sort(comparator);
            return taskList;
        }
        if (TypeOfSort.DATE_OF_CREATE.displayName().equals(typeOfSort.toUpperCase())) {
            @NotNull Comparator<Task> comparator = new ComparatorByDateOfCreate<>();
            taskList.sort(comparator);
            return taskList;
        }
        if (TypeOfSort.DATE_OF_BEGIN.displayName().equals(typeOfSort.toUpperCase())) {
            @NotNull Comparator<Task> comparator = new ComparatorByDateOfBegin<>();
            taskList.sort(comparator);
            return taskList;
        }
        if (TypeOfSort.DATE_OF_END.displayName().equals(typeOfSort.toUpperCase())) {
            @NotNull Comparator<Task> comparator = new ComparatorByDateOfEnd<>();
            taskList.sort(comparator);
            return taskList;
        } else throw new CommandCorruptException();
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId(@NotNull final String userId) {
        final List<Task> tasks = new ArrayList<>();

        for (Map.Entry<String, Task> entry : getMap().entrySet()) {
            if (userId.equals(entry.getValue().getUserId())) {
                tasks.add(entry.getValue());
            }
        }
        return tasks;
    }

    @Nullable
    @Override
    public String getTaskIdByName(@NotNull final String taskName) {
        for (Map.Entry<String, Task> entry : getMap().entrySet()) {
            if (taskName.equals(entry.getValue().getName())) {
                return entry.getKey();
            }
        }
        return null;
    }

    @NotNull
    @Override
    public List<String> showAllTasksOfProject(@NotNull final String projectId) {
        final List<String> listOfTasks = new ArrayList<>();

        for (Map.Entry<String, Task> entry : getMap().entrySet()) {
            if (projectId.equals(entry.getValue().getProjectId())) {
                listOfTasks.add(entry.getValue().getName());
            }
        }
        return listOfTasks;
    }

    @NotNull
    @Override
    public List<Task> search(@Nullable final String userId, @NotNull final String partOfData) {
        final List<Task> taskList = new ArrayList<>();
        for (Map.Entry<String, Task> entry : getMap().entrySet()) {
            @NotNull final Task task = entry.getValue();
            if (task.getName() == null) continue;
            if (task.getName().contains(partOfData)) {
                taskList.add(task);
            }
            if (task.getDescription() == null) continue;
            if (task.getDescription().contains(partOfData)) {
                taskList.add(task);
            }
        }
        return taskList;
    }

    @Override
    public void persist(@NotNull final Task task) {
        if (isExist(task)) return;
        task.setStatus(Status.PLANNED);
        getMap().put(task.getId(), task);
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final List<Task> tasksToRemove = findAllByUserId(userId);
        for (@NotNull Task task : tasksToRemove) {
            for (Map.Entry<String, Task> entry : getMap().entrySet()) {
                if (task.getId().equals(entry.getKey())) {
                    remove(task);
                }
            }
        }
    }

    @Override
    public void update(@NotNull final Task task) {
        for (Map.Entry<String, Task> entry : getMap().entrySet()) {
            if (task.getId().equals(entry.getKey())) {
                entry.getValue().setName(task.getName());
                entry.getValue().setDescription(task.getDescription());
                entry.getValue().setProjectId(task.getProjectId());
                entry.getValue().setDateOfBegin(task.getDateOfBegin());
                entry.getValue().setDateOfEnd(task.getDateOfEnd());
                entry.getValue().setStatus(task.getStatus());
            }
        }
    }

    @Override
    public void removeAllTasksOfProject(@NotNull final String projectId) {
        getMap().entrySet().removeIf(entry -> projectId.equals(entry.getValue().getProjectId()));
    }
}
