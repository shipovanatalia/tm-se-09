package ru.shipova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.repository.IProjectRepository;
import ru.shipova.tm.comparator.ComparatorByDateOfBegin;
import ru.shipova.tm.comparator.ComparatorByDateOfCreate;
import ru.shipova.tm.comparator.ComparatorByDateOfEnd;
import ru.shipova.tm.comparator.ComparatorByStatus;
import ru.shipova.tm.constant.Status;
import ru.shipova.tm.constant.TypeOfSort;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.exception.CommandCorruptException;

import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public ProjectRepository() {
        //Таски для проверки работы сортировщика
        final String project_1_Id = UUID.randomUUID().toString();
        final String project_1_userId = "1";
        final String project_1_name = "project_1";
        final Status project_1_status = Status.IN_PROCESS;
        final Date project_1_date_of_begin = new Date(2019, 1, 1);
        final Date project_1_date_of_end = new Date(2019, 10, 1);
        getMap().put(project_1_Id, new Project(project_1_Id, project_1_userId, project_1_name,
                project_1_status, project_1_date_of_begin, project_1_date_of_end));

        final String project_2_Id = UUID.randomUUID().toString();
        final String project_2_userId = "1";
        final String project_2_name = "project_2";
        final Status project_2_status = Status.READY;
        final Date project_2_date_of_begin = new Date(2019, 2, 1);
        final Date project_2_date_of_end = new Date(2019, 9, 1);
        getMap().put(project_2_Id, new Project(project_2_Id, project_2_userId, project_2_name,
                project_2_status, project_2_date_of_begin, project_2_date_of_end));

        final String project_3_Id = UUID.randomUUID().toString();
        final String project_3_userId = "1";
        final String project_3_name = "project_3";
        final Status project_3_status = Status.PLANNED;
        final Date project_3_date_of_begin = new Date(2019, 3, 1);
        final Date project_3_date_of_end = new Date(2019, 8, 1);
        getMap().put(project_3_Id, new Project(project_3_Id, project_3_userId, project_3_name,
                project_3_status, project_3_date_of_begin, project_3_date_of_end));
    }

    @NotNull
    @Override
    public List<Project> findAllByUserId(@NotNull final String userId) {
        final List<Project> projects = new ArrayList<>();

        for (Map.Entry<String, Project> entry : getMap().entrySet()) {
            if (userId.equals(entry.getValue().getUserId())) {
                projects.add(entry.getValue());
            }
        }
        return projects;
    }

    @NotNull
    @Override
    public String getProjectIdByName(@NotNull final String projectName) {
        for (Map.Entry<String, Project> entry : getMap().entrySet()) {
            if (projectName.equals(entry.getValue().getName())) {
                return entry.getKey();
            }
        }
        return "";
    }

    @NotNull
    @Override
    public List<Project> search(@Nullable final String userId, @NotNull final String partOfData) {
        final List<Project> projectList = new ArrayList<>();
        for (Map.Entry<String, Project> entry : getMap().entrySet()) {
            @NotNull final Project project = entry.getValue();
            if (project.getName() == null) continue;
            if (project.getName().contains(partOfData)) {
                projectList.add(project);
            }
            if (project.getDescription() == null) continue;
            if (project.getDescription().contains(partOfData)) {
                projectList.add(project);
            }
        }
        return projectList;
    }

    @NotNull
    public List<Project> sort(@NotNull final String typeOfSort,
                              @NotNull final List<Project> projectList) throws CommandCorruptException {
        if (TypeOfSort.NO.displayName().equals(typeOfSort.toUpperCase())){
            return projectList;
        }
        if (TypeOfSort.STATUS.displayName().equals(typeOfSort.toUpperCase())) {
            Comparator<Project> comparator = new ComparatorByStatus<Project>();
            projectList.sort(comparator);
            return projectList;
        }
        if (TypeOfSort.DATE_OF_CREATE.displayName().equals(typeOfSort.toUpperCase())){
            Comparator<Project> comparator = new ComparatorByDateOfCreate<>();
            projectList.sort(comparator);
            return projectList;
        }
        if (TypeOfSort.DATE_OF_BEGIN.displayName().equals(typeOfSort.toUpperCase())){
            Comparator<Project> comparator = new ComparatorByDateOfBegin<>();
            projectList.sort(comparator);
            return projectList;
        }
        if (TypeOfSort.DATE_OF_END.displayName().equals(typeOfSort.toUpperCase())){
            Comparator<Project> comparator = new ComparatorByDateOfEnd<>();
            projectList.sort(comparator);
            return projectList;
        }
        else throw new CommandCorruptException();
    }

    @Override
    public void persist(@NotNull final Project project) {
        if (isExist(project)) return;
        project.setStatus(Status.PLANNED);
        getMap().put(project.getId(), project);
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final List<Project> projectsToRemove = findAllByUserId(userId);
        for (Project project : projectsToRemove) {
            for (Map.Entry<String, Project> entry : getMap().entrySet()) {
                if (project.getId().equals(entry.getKey())) {
                    remove(project);
                }
            }
        }
    }

    @Override
    public void update(@NotNull final Project project) {
        for (Map.Entry<String, Project> entry : getMap().entrySet()) {
            if (project.getId().equals(entry.getKey())) {
                entry.getValue().setName(project.getName());
                entry.getValue().setDescription(project.getDescription());
                entry.getValue().setDateOfCreate(project.getDateOfCreate());
                entry.getValue().setDateOfEnd(project.getDateOfEnd());
                entry.getValue().setStatus(project.getStatus());
            }
        }
    }
}
