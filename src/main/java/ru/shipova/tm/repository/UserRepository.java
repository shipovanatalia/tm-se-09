package ru.shipova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.repository.IUserRepository;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.util.PasswordHashUtil;

import java.util.Map;

import static ru.shipova.tm.constant.RoleType.ADMIN;
import static ru.shipova.tm.constant.RoleType.USER;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository() {
        final String userId = "1";
        final String userLogin = "user";
        final String userPasswordHash = PasswordHashUtil.md5("user");
        getMap().put(userId, new User (userId,userLogin, userPasswordHash, USER));

        final String adminId = "2";
        final String adminLogin = "admin";
        final String adminPasswordHash = PasswordHashUtil.md5("admin");
        getMap().put(adminId, new User(adminId, adminLogin, adminPasswordHash, ADMIN));
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        String userId = "";
        for (Map.Entry<String, User> entry : getMap().entrySet()) {
            if (login.equals(entry.getValue().getLogin())) {
                userId = entry.getKey();
            }
        }
        if (!getMap().containsKey(userId)) return null;
        return getMap().get(userId);
    }

    @Override
    public void setNewPassword(@Nullable final String login, @Nullable final String passwordHash) {
        if (login == null || login.isEmpty()) return;
        @Nullable final User user = findByLogin(login);
        if (user == null || !isExist(user)) return;

        for (Map.Entry<String, User> entry : getMap().entrySet()) {
            if (user.getId().equals(entry.getKey())) {
                entry.getValue().setPasswordHash(passwordHash);
            }
        }
    }

    @Override
    public void updateUser(@Nullable final String login, @Nullable final RoleType roleType){
        if (login == null || login.isEmpty()) return;
        @Nullable final User user = findByLogin(login);
        if (user == null || !isExist(user)) return;
        for (Map.Entry<String, User> entry : getMap().entrySet()) {
            if (user.getId().equals(entry.getKey())) {
                entry.getValue().setRoleType(roleType);
            }
        }
    }
}
