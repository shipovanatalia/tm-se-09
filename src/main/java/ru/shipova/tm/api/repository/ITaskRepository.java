package ru.shipova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.exception.CommandCorruptException;

import java.util.List;

public interface ITaskRepository {
    @Nullable Task findOne(@NotNull final String taskId);

    @Nullable List<Task> findAll();

    @Nullable List<Task> findAllByUserId(@NotNull final String userId);

    @NotNull List<Task> sort(@NotNull final String typeOfSort, @NotNull List<Task> taskList) throws CommandCorruptException;

    @NotNull List<Task> search(@Nullable final String userId, @NotNull final String partOfData);

    @Nullable String getTaskIdByName(@NotNull final String taskName);

    /**
     * Метод вставляет новый объект, если его не было. Данные объекта не обновляет и не перезатирает.
     *
     * @param task - объект, который необходимо вставить
     */

    void persist(@NotNull final Task task);

    /**
     * Метод вставляет новый объект, если его не было.
     * Если объект был, он его обновляет.
     *
     * @param task - объект, который необходимо вставить
     */
    void merge(@NotNull final Task task);

    void remove(@NotNull final String taskId);

    void remove(@NotNull final Task task);

    void removeAllByUserId(@NotNull final String userId);

    /**
     * Метод находит объект с идентичным id и обновляет все его поля на поля объекта task.
     *
     * @param task - объект с новыми данными.
     */
    void update(@NotNull final Task task);

    @NotNull List<String> showAllTasksOfProject(@NotNull final String projectId);

    void removeAllTasksOfProject(@NotNull final String projectId);
}
