package ru.shipova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.exception.CommandCorruptException;

import java.util.List;

public interface IProjectRepository {
    @Nullable Project findOne(@NotNull final String projectId);

    @Nullable List<Project> findAll();

    @Nullable List<Project> findAllByUserId(@NotNull final String userId);

    @NotNull List<Project> sort(@NotNull final String typeOfSort, @NotNull List<Project> projectList) throws CommandCorruptException;

    @NotNull String getProjectIdByName(@NotNull final String projectName);

    @NotNull List<Project> search(@Nullable final String userId, @NotNull final String partOfData);

    void persist(@NotNull final Project project);

    void merge(@NotNull final Project project);

    void remove(@NotNull final String projectId);

    void remove(@NotNull final Project project);

    void removeAllByUserId(@NotNull final String userId);

    void update(@NotNull final Project project);
}
