package ru.shipova.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.service.TerminalService;

public interface IServiceLocator {
    @NotNull TerminalService getTerminalService();
    @NotNull IProjectService getIProjectService();
    @NotNull ITaskService getITaskService();
    @NotNull IUserService getIUserService();
}
