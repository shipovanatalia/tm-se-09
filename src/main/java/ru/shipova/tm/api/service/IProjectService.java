package ru.shipova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.exception.CommandCorruptException;

import java.util.List;

public interface IProjectService {

    @Nullable List<Project> getListProject(@Nullable final String userId);

    @NotNull List<Project> sort(@NotNull final String typeOfSort, @NotNull final List<Project> projectList) throws CommandCorruptException;

    @Nullable List<Project> search(String userId, String partOfData);

    void create(@NotNull final String userId, @NotNull final String projectName);

    void clear(@Nullable final String userId);

    void remove(@Nullable final String userId, @Nullable final String projectName);
}
