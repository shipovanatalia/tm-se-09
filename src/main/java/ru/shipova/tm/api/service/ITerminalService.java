package ru.shipova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.command.AbstractCommand;

import java.util.List;

public interface ITerminalService {
    @NotNull List<AbstractCommand> getCommands();

    @NotNull String nextLine();
}
