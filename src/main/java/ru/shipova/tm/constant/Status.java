package ru.shipova.tm.constant;

import org.jetbrains.annotations.NotNull;

public enum Status {
    PLANNED("ЗАПЛАНИРОВАННО"),
    IN_PROCESS("В ПРОЦЕССЕ"),
    READY("ГОТОВО");

    private @NotNull final String name;

    Status(@NotNull final String name) {
        this.name = name;
    }

    @NotNull public String displayName() {
        return name;
    }
}
