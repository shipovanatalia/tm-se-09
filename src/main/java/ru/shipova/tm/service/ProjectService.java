package ru.shipova.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.api.repository.IProjectRepository;
import ru.shipova.tm.api.repository.ITaskRepository;
import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.exception.CommandCorruptException;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
public final class ProjectService implements IProjectService {
    private final IProjectRepository projectRepository;
    private final ITaskRepository taskRepository;

    @Nullable
    @Override
    public List<Project> getListProject(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<Project> sort(@NotNull final String typeOfSort, @NotNull final List<Project> projectList) throws CommandCorruptException {
        return projectRepository.sort(typeOfSort, projectList);
    }

    @Nullable
    @Override
    public List<Project> search(@Nullable final String userId, @Nullable final String partOfData) {
        if (userId == null || userId.isEmpty()) return null;
        if (partOfData == null || partOfData.isEmpty()) return null;
        return projectRepository.search(userId, partOfData);
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final String projectName) {
        @NotNull final String projectId = UUID.randomUUID().toString();
        projectRepository.persist(new Project(projectId, userId, projectName));
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.removeAllByUserId(userId);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectName) {
        if (projectName == null || projectName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;

        @Nullable final String projectId = projectRepository.getProjectIdByName(projectName);

        @Nullable final Project project = projectRepository.findOne(projectId);
        if (project == null) return;
        if (!userId.equals(project.getUserId())) return;

        projectRepository.remove(projectId);
        taskRepository.removeAllTasksOfProject(projectId);
    }
}
