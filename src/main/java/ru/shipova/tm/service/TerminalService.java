package ru.shipova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.api.service.ITerminalService;
import ru.shipova.tm.bootstrap.Bootstrap;
import ru.shipova.tm.command.AbstractCommand;

import java.util.List;
import java.util.Scanner;

public final class TerminalService implements ITerminalService {
    private @NotNull final Bootstrap bootstrap;
    private final Scanner in = new Scanner(System.in);

    public TerminalService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @NotNull
    @Override
    public List<AbstractCommand> getCommands(){
        return bootstrap.getCommands();
    }

    @NotNull
    @Override
    public String nextLine(){
        return in.nextLine();
    }
}
