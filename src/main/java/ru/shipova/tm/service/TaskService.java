package ru.shipova.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.api.repository.IProjectRepository;
import ru.shipova.tm.api.repository.ITaskRepository;
import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.exception.CommandCorruptException;
import ru.shipova.tm.exception.ProjectDoesNotExistException;

import javax.annotation.Nullable;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
public final class TaskService implements ITaskService {
    private @NotNull
    final ITaskRepository taskRepository;

    private @NotNull
    final IProjectRepository projectRepository;

    @NotNull
    @Override
    public List<String> showAllTasksOfProject(@NotNull final String projectName) throws ProjectDoesNotExistException {
        if (projectName.isEmpty()) throw new ProjectDoesNotExistException();

        @NotNull final String projectId = projectRepository.getProjectIdByName(projectName);
        if (projectRepository.findOne(projectId) == null) throw new ProjectDoesNotExistException();

        return taskRepository.showAllTasksOfProject(projectId);
    }

    @Nullable
    @Override
    public List<Task> getListTask(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    @NotNull
    public List<Task> sort(@NotNull final String typeOfSort, @NotNull final List<Task> taskList) throws CommandCorruptException {
        return taskRepository.sort(typeOfSort, taskList);
    }

    @Nullable
    @Override
    public List<Task> search(@Nullable final String userId, @Nullable final String partOfData) {
        if (userId == null || userId.isEmpty()) return null;
        if (partOfData == null || partOfData.isEmpty()) return null;
        return taskRepository.search(userId, partOfData);
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String taskName, @Nullable final String projectName) throws ProjectDoesNotExistException {
        if (userId == null || userId.isEmpty()) return;
        if (taskName == null || taskName.isEmpty()) return;
        if (projectName == null || projectName.isEmpty()) return;

        @Nullable final String projectId = projectRepository.getProjectIdByName(projectName);
        if (projectRepository.findOne(projectId) == null) throw new ProjectDoesNotExistException();

        @NotNull final String taskId = UUID.randomUUID().toString();
        taskRepository.persist(new Task(taskId, taskName, projectId, userId));
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeAllByUserId(userId);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskName) {
        if (taskName == null || taskName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;

        @Nullable final String taskId = taskRepository.getTaskIdByName(taskName);
        @Nullable final Task task = taskRepository.findOne(taskId);
        if (task == null) return;
        task.setDateOfEnd(new Date());
        if (!userId.equals(task.getUserId())) return;
        taskRepository.remove(taskId);
    }
}
