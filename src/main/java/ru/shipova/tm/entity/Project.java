package ru.shipova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.constant.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractEntity {

    @Nullable
    private String name;

    @Nullable
    private String userId;

    @Nullable
    private String description;

    @Nullable
    private Date dateOfBegin;

    @Nullable
    private Date dateOfEnd;

    @Nullable
    private Status status;

    public Project(@NotNull final String id, @Nullable final String userId, @Nullable final String name) {
        this.id = id;
        this.name = name;
        this.userId = userId;
    }

    //костыльный конструктор для проверки сортировки
    public Project(String id, String userId, String name, Status status, Date dateOfbegin, Date dateOfEnd) {
        this(id, userId, name);
        this.dateOfBegin = dateOfbegin;
        this.dateOfEnd = dateOfEnd;
        this.status = status;
    }
}
