package ru.shipova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.constant.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractEntity {

    @Nullable
    private String projectId;

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Date dateOfCreate;

    @Nullable
    private Date dateOfBegin;

    @Nullable
    private Date dateOfEnd;

    @Nullable
    private String userId;

    @Nullable
    private Status status;

    public Task(@NotNull final String id,
                @Nullable final String name,
                @Nullable final String projectId,
                @Nullable final String userId) {
        this.id = id;
        this.name = name;
        this.projectId = projectId;
        this.userId = userId;
        this.dateOfCreate = new Date();
    }

    //костыльный конструктор для проверки сортировки
    public Task(String id, String name, String projectId, String userId, Status status, Date dateOfbegin, Date dateOfEnd) {
        this(id, name, projectId, userId);
        this.dateOfBegin = dateOfbegin;
        this.dateOfEnd = dateOfEnd;
        this.status = status;
    }
}
