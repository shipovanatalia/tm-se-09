package ru.shipova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.constant.RoleType;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractEntity{

    @Nullable
    private String login;

    @Nullable
    private RoleType roleType;

    @Nullable
    private String passwordHash;

    public User(@NotNull final String id, @Nullable final String login, @Nullable final String passwordHash, @Nullable final RoleType roleType) {
        this.id = id;
        this.login = login;
        this.roleType = roleType;
        this.passwordHash = passwordHash;
    }
}
