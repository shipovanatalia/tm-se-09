package ru.shipova.tm.comparator;

import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.constant.Status;
import ru.shipova.tm.entity.AbstractEntity;

import java.util.Comparator;


public class ComparatorByStatus<T extends AbstractEntity> implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        @Nullable final Status firstStatus = ((AbstractEntity) o1).getStatus();
        @Nullable final Status secondStatus = ((AbstractEntity) o2).getStatus();
        if (firstStatus == null) return -1;
        if (secondStatus == null) return 1;
        return (firstStatus.ordinal() - (secondStatus.ordinal()));
    }
}
