package ru.shipova.tm.comparator;

import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.entity.AbstractEntity;

import java.util.Comparator;
import java.util.Date;

public class ComparatorByDateOfBegin<T extends AbstractEntity> implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        @Nullable final Date firstDate = ((AbstractEntity) o1).getDateOfBegin();
        @Nullable final Date secondDate = ((AbstractEntity) o2).getDateOfBegin();
        if (firstDate == null) return -1;
        if (secondDate == null) return 1;
        return (firstDate.compareTo(secondDate));
    }
}
