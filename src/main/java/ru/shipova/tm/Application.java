package ru.shipova.tm;

import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.bootstrap.Bootstrap;

public final class Application {
    public static void main(String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
